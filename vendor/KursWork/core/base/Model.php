<?php


namespace KursWork\base;

use KursWork\Db;
use Valitron\Validator;

abstract  class Model //Работа с базой даных(и другими даными)
{
    public $attributes=[]; //
    public $errors=[]; // массив для хранение ошибок
    public  $rules=[]; //правила валидации данных

    public function __construct()
{
    Db::instance();
}
    public function load($data){
        foreach ($this->attributes as $name => $value){
            if(isset($data[$name])){
                $this->attributes[$name]=$data[$name];
            }
        }
    }
    public function save($table){
        $tbl = \R::dispense($table);
        foreach ($this->attributes as $name =>$value){
            $tbl->$name = $value;
        }
        return \R::store($tbl);

    }
    public function update($table,$id){
        $bean = \R::load($table,$id);
        foreach ($this->attributes as $name =>$value){
            $bean->$name = $value;
        }
        return \R::store($bean);

    }
    public function validate($data){
        Validator::langDir(WWW.'/validator/lang');
        Validator::lang('ru');
        $v = new Validator($data);
        $v->rules($this->rules);
        if($v->validate()){
            return true;
        }
        $this->errors = $v->errors();
        return false;

    }
    public function getErrors(){
        $errors='<ul>';
        foreach ($this->errors as $error){
            foreach ($error as $item){
                $errors .="<li>$item</li>";
            }
        }
        $errors.='</ul>';
        $_SESSION['error']= $errors;
    }
}