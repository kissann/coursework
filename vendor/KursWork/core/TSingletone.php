<?php


namespace KursWork;


trait TSingletone
{

    private static $instance;

    public static function instance(){
        if(self::$instance===null){ //if свойство пусто
            self::$instance=new self; //ложим в него объект и возвращаем
        }
        return self::$instance; // или возвращаем
    }


}