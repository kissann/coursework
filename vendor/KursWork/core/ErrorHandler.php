<?php


namespace KursWork;


class ErrorHandler
{
    public function __construct()
    {

        if(DEBUG){
            error_reporting(-1);//показывать ошибки
        }else{
            error_reporting(0);//не показывать
        }
        set_exception_handler([$this,'exeptionHandler']);
    }

    public function exeptionHandler($e){//вызов
        $this->logErrors($e->getMessage(),$e->getFile(),$e->getLine());
        $this->displayError('Исключение',$e->getMessage(),$e->getFile(),$e->getLine(),
            $e->getCode());
    }

    protected function logErrors($message='',$file='',$line=''){
        //формирование сообщения об ошибке и запись в лог
        error_log("[".date('Y-m-d H:i:s')."]Текст ошибки:{$message} | Файл:{$file} | Строка: {$line}\n================
        \n",3,ROOT.'/tmp/errors.log');
    }

    protected function displayError($errno,$errstr,$errfile,$errline,$responce=404){ //отображение ошибки в браузере
            http_response_code($responce);
            if($responce==404 && !DEBUG){
                require  WWW.'/errors/404.php';
                die;
            }
            if(DEBUG){
                require  WWW.'/errors/dev.php';
            }else{
                require  WWW.'/errors/prod.php';
            }
            die;
    }
}