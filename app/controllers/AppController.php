<?php


namespace app\controllers;


use app\models\AppModel;
use app\widgets\currency\Currency;
use KursWork\App;
use KursWork\base\Controller;
use KursWork\Cache;

class AppController extends Controller
{
    public function __construct($route)
    {
        parent::__construct($route);
        new AppModel();
       App::$app->setProperty('currencies', Currency::getCurrencies());//запись в реестр-валют
       App::$app->setProperty('currency',Currency::getCurrency(App::$app->getProperty('currencies')));//запись в реестр текущей валюты
        App::$app->setProperty('cats',self::cacheCategory());
    }
    public static function cacheCategory(){ //получение данных про категории с кеша или бд
        $cache = Cache::instance();
        $cats = $cache->get('cats');
        if(!$cats){
            $cats = \R::getAssoc("SELECT * FROM category");
            $cache->set('cats',$cats);
        }
        return $cats;

    }

}